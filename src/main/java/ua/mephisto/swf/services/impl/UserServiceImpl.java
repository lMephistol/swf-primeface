package ua.mephisto.swf.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.log4j.Logger;
import org.primefaces.component.inputtext.InputText;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ua.mephisto.swf.dao.UserDao;
import ua.mephisto.swf.domain.UserEntity;
import ua.mephisto.swf.services.UserService;

/**
 * Service providing service methods to work with user data and entity.
 * 
 * Created by Mephisto on 22.07.2015.
 */
public class UserServiceImpl implements UserService, UserDetailsService {

	private UserDao userDao;
	private static Logger logger = Logger.getLogger(UserServiceImpl.class);

	public boolean createUser(UserEntity userEntity) {

		if(!userDao.checkAvailable(userEntity.getUserName())){

			FacesMessage message = constructErrorMessage(String.format(getMessageBundle().getString("userExistMsg"), userEntity.getUserName()), null);
			getFacesContext().addMessage(null, message);
			logger.error("User with user name " + userEntity.getUserName() + " already exist" );
			return false;
		}try{
			// Save the password to pass validation account after creating.
			String password = userEntity.getPassword();

			userEntity.setPassword(new BCryptPasswordEncoder().encode(password));
			userDao.save(userEntity);
			userEntity.setPassword(password);
			logger.info("User was created with user name " + userEntity.getUserName());
		}catch(Exception e){

			getFacesContext().addMessage(null,constructFatalMessage(e.getMessage(), null));

			return false;
		}

		return true;
	}

	public boolean checkAvailable(AjaxBehaviorEvent event) {
		
		InputText inputText = (InputText) event.getSource();
		String value = (String) inputText.getValue();
		
		boolean available = userDao.checkAvailable(value);
		
		if (!available) {
			FacesMessage message = constructErrorMessage(null, String.format(getMessageBundle().getString("userExistsMsg"), value));
			getFacesContext().addMessage(event.getComponent().getClientId(), message);
		} else {
			FacesMessage message = constructInfoMessage(null, String.format(getMessageBundle().getString("userAvailableMsg"), value));
			getFacesContext().addMessage(event.getComponent().getClientId(), message);
		}
		
		return available;
	}

	/**
	 * Construct UserDetails instance required by spring security
	 */
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		UserEntity user = userDao.loadUserByUserName(userName);
		if(user == null){
			throw new UsernameNotFoundException(String.format(getMessageBundle().getString("badCredentials"), userName));
		}
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		User userDetails = new User(user.getUserName(), user.getPassword(), authorities);
		logger.info("User was loaded by user name " + userName + "with ROLE_USER");
		return userDetails;
	}

/*
//	Procedure checking the authenticated user credential after a while.
	public void processCheckUserValidation(HtmlInputSecret is) {
		String password = (String)is.getSubmittedValue();
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		UserEntity user = userDao.loadUserByUserName(userName);
		BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
		if(!enc.matches(password, user.getPassword())){
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("../app/logout");
			} catch (IOException e) {
				logger.error("Redirect error" + e.getMessage());
			}
			logger.error("User validation error for user " + userName);
		}
		logger.info("User validation is complete for user " + userName);
	}
*/


	public UserEntity loadUserEntityByUsername(String userName) {
		return userDao.loadUserByUserName(userName);
	}


	protected FacesMessage constructErrorMessage(String message, String detail){
		return new FacesMessage(FacesMessage.SEVERITY_ERROR, message, detail);
	}
	
	protected FacesMessage constructInfoMessage(String message, String detail) {
		return new FacesMessage(FacesMessage.SEVERITY_INFO, message, detail);
	}
	
	protected FacesMessage constructFatalMessage(String message, String detail) {
		return new FacesMessage(FacesMessage.SEVERITY_FATAL, message, detail);
	}
	
	protected FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	protected ResourceBundle getMessageBundle() {
		return ResourceBundle.getBundle("message-labels");
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
