package ua.mephisto.swf.services.impl;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import ua.mephisto.swf.domain.UserEntity;
import ua.mephisto.swf.services.UserAuthenticationProviderService;

/**
 * Provides processing service to set user authentication session
 * 
 * Created by Mephisto on 22.07.2015.
 */
public class UserAuthenticationProviderServiceImpl implements UserAuthenticationProviderService {

	private AuthenticationManager authenticationManager;

	private static Logger logger = Logger.getLogger(UserAuthenticationProviderServiceImpl.class);
	
	/**
	 * Process user authentication
	 * 
	 * @param user
	 * @return
	 */
	public boolean processUserAuthentication(UserEntity user) {
		try{
			Authentication request = new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPassword());
			Authentication result = authenticationManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
			logger.info(user.getUserName() + " was authentificated");
			return true;
		}catch(AuthenticationException e){
			logger.error(user.getUserName() + " wasn't authentificated");
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Sorry!"));
			return false;
		}
	}

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

}
