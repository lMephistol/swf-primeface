package ua.mephisto.swf.services;

import ua.mephisto.swf.domain.UserEntity;

/**
 * Provides processing service to set user authentication session
 *
 * Created by Mephisto on 22.07.2015.
 */
public interface UserAuthenticationProviderService {

	/**
	 * Process user authentication
	 * 
	 * @param user
	 * @return
	 */
	boolean processUserAuthentication(UserEntity user);
}
