package ua.mephisto.swf.services;

import javax.faces.event.AjaxBehaviorEvent;

import ua.mephisto.swf.domain.UserEntity;

/**
 * Service providing service methods to work with user data and entity.
 *
 * Created by Mephisto on 22.07.2015.
 */
public interface UserService {

	/**
	 * Create user - persist to database
	 * 
	 * @param userEntity
	 * @return true if success
	 */
	boolean createUser(UserEntity userEntity);
	
	/**
	 * Check user name availability. UI ajax use.
	 * 
	 * @param event ajax
	 * @return
	 */
	boolean checkAvailable(AjaxBehaviorEvent event);
	
	/**
	 * Retrieves full User record from database by user name
	 * 
	 * @param userName
	 * @return UserEntity
	 */
	UserEntity loadUserEntityByUsername(String userName);
}
