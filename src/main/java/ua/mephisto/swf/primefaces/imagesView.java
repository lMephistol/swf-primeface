package ua.mephisto.swf.primefaces;

import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ua.mephisto.swf.dao.UserDao;
import ua.mephisto.swf.domain.ImageEntity;
import ua.mephisto.swf.domain.UserEntity;
import ua.mephisto.swf.primefaces.upload.UploadImage;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * JSF bean for works with image upload/delete
 *
 * Created by mephisto on 05.10.15.
 */
@Component
@ManagedBean
@RequestScoped
public class imagesView {
    @Autowired
    @Qualifier ("imgurUploader")
    private UploadImage uploaderImages;
    @Autowired
    private UserDao userDao;
    private List<ImageEntity> images = new ArrayList<ImageEntity>();

    public List<ImageEntity> getImages() {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        UserEntity userEntity = userDao.loadUserByUserName(userName);
        if(userEntity.getImages()!=null){
            images = userEntity.getImages();
        }
        return images;
    }

    public void upload(FileUploadEvent event) {
        try {
            ImageEntity imageEntity = uploaderImages.upload(event);
            String userName = SecurityContextHolder.getContext().getAuthentication().getName();
            UserEntity userEntity = userDao.loadUserByUserName(userName);
            imageEntity.setUserEntity(userEntity);
            userEntity.addImage(imageEntity);
            userDao.update(userEntity);
            images.add(imageEntity);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Upload successful","File uploaded"));
        } catch (IOException e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "File wasn't uploaded"));
        }
    }
    public void remove(ImageEntity image){
        try {
            String userName = SecurityContextHolder.getContext().getAuthentication().getName();
            UserEntity userEntity = userDao.loadUserByUserName(userName);
            this.images.remove(image);
            uploaderImages.removeImage(image);
            userEntity.deleteImage(image);
            userDao.update(userEntity);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Delete successful", "File deleted"));
        } catch (IOException e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "File wasn't deleted"));
        }

    }
}
