package ua.mephisto.swf.primefaces.upload.impl;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.mephisto.swf.domain.ImageEntity;
import ua.mephisto.swf.primefaces.upload.UploadImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Base64;

/**
 * Concret uploader for imgur.com.
 *
 * Created by mephisto on 05.10.15.
 */
@Component(value = "imgurUploader")
public class UploadToImgur extends UploadImage {


    @Value("${imgurClientId:5ab1a99559b2bfa}")
    private String imgurClientId;
    private final static String IMGUR_URL = "https://api.imgur.com/3/image";

    @Override
    protected ImageEntity concrectUpload(InputStream is, String basename, String extension) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        BufferedImage bufferedImage = ImageIO.read(is);
        ImageIO.write(bufferedImage, extension, byteArray);
        byte[] byteImage = byteArray.toByteArray();
        String dataImage = Base64.getEncoder().encodeToString(byteImage);
        String data = URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(dataImage, "UTF-8") + "&" +
                URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(basename, "UTF-8");
        URL url = new URL(IMGUR_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Authorization", "Client-ID " + getImgurClientId());

        connection.connect();
        StringBuilder stb = new StringBuilder();
        try (OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream())) {
            wr.write(data);
            wr.flush();
            try (BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                String line;
                while ((line = rd.readLine()) != null) {
                    stb.append(line).append("\n");
                }
            }
        }
        connection.disconnect();
        JsonNode root = objectMapper.readTree(stb.toString());
        ImageEntity imageEntity = objectMapper.readValue(root.get("data"), ImageEntity.class);
        imageEntity.setFormat(extension);
        imageEntity.setTitle(basename);
        return imageEntity;
    }

    @Override
    public void removeImage(ImageEntity image) throws IOException {
        String deletehash = image.getDeletehash();
        URL url;
        url = new URL(IMGUR_URL + "/" + deletehash);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setRequestMethod("DELETE");
        connection.setRequestProperty("Authorization", "Client-ID " + imgurClientId);

        connection.connect();
        StringBuilder stb = new StringBuilder();
        try (BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            while ((line = rd.readLine()) != null) {
                stb.append(line).append("\n");
            }
        }
        connection.disconnect();
    }


    public String getImgurClientId() {
        return imgurClientId;
    }

    public void setImgurClientId(String imgurClientId) {
        this.imgurClientId = imgurClientId;
    }
}
