package ua.mephisto.swf.primefaces.upload;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;

import ua.mephisto.swf.domain.ImageEntity;

import java.io.IOException;
import java.io.InputStream;

/**
 * Main class for uploader implementation.
 *
 * Created by mephisto on 05.10.15.
 */
public abstract class UploadImage {

    public ImageEntity upload(FileUploadEvent event) throws IOException {
        String fullName = event.getFile().getFileName();
        String baseName = FilenameUtils.getBaseName(fullName);
        String extension = FilenameUtils.getExtension(fullName);
        InputStream is = event.getFile().getInputstream();
        ImageEntity imageEntity = concrectUpload(is, baseName, extension);
        return imageEntity;
    }

    protected abstract ImageEntity concrectUpload(InputStream is, String basename, String extension) throws IOException;


    public abstract void removeImage(ImageEntity image) throws IOException;

}
