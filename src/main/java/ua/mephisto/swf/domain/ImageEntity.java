package ua.mephisto.swf.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 *  Entity to hold Image data
 *
 * Created by mephisto on 28.09.15.
 */
@Entity
@Table(name="appuser_images")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageEntity implements Serializable{
    private static final long serialVersionUID = -4789920463309744548L;

    @Id
    private String id;
    private String title;
    private Integer datetime;
    private Integer width;
    private Integer height;
    private String format;
    private String deletehash;
    private String link;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDatetime() {
        return datetime;
    }

    public void setDatetime(Integer datetime) {
        this.datetime = datetime;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDeletehash() {
        return deletehash;
    }

    public void setDeletehash(String deletehash) {
        this.deletehash = deletehash;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    @Override
    public boolean equals(Object object){
        if(object == this)
            return true;
        if(object == null || object.getClass() != this.getClass())
            return false;
        ImageEntity temp = (ImageEntity) object;
        return getId().equals(temp.getId());
    }
    @Override
    public int hashCode(){
        return getId().hashCode();
    }
}
