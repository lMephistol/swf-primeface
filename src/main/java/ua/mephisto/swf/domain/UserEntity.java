package ua.mephisto.swf.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Entity to hold application user data
 * 
 * Created by Mephisto on 22.07.2015.
 */
@Entity
@Table(name="appuser")
public class UserEntity implements Serializable{
	private static final long serialVersionUID = -8789920463809744548L;
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	@OneToMany(mappedBy = "userEntity",cascade = CascadeType.ALL,fetch = FetchType.EAGER,orphanRemoval = true)
	private List<ImageEntity> images = new ArrayList<ImageEntity>();

	public List<ImageEntity> getImages() {
		return images;
	}

	public void setImages(List<ImageEntity> images) {
		this.images = images;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public void addImage(ImageEntity imageEntity){
		imageEntity.setUserEntity(this);
		this.images.add(imageEntity);
	}
	public void deleteImage(ImageEntity imageEntity){
		this.images.remove(imageEntity);
		imageEntity.setUserEntity(null);
	}
}
