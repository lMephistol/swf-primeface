package ua.mephisto.swf.dao;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.util.Assert;

import ua.mephisto.swf.domain.ImageEntity;
import ua.mephisto.swf.domain.UserEntity;

/**
 * Data access object to work with User entity database operations.
 * 
 * Created by Mephisto on 22.07.2015.
 */
public class UserDao extends GenericJpaDao<UserEntity, Long>{

	public UserDao() {
		super(UserEntity.class);
	}

	public boolean checkAvailable(String userName) {
		Assert.notNull(userName);
		
		Query query = getEntityManager()
				.createQuery("select count(*) from " + getPersistentClass().getSimpleName()
						+ " u where u.userName = :userName").setParameter("userName", userName);
		
		Long count = (Long) query.getSingleResult();
		
		return count < 1;
	}

	public UserEntity loadUserByUserName(String userName) {
		Assert.notNull(userName);
		
		UserEntity user = null;

		Query query = getEntityManager().createQuery("select u from " + getPersistentClass().getSimpleName()
				+ " u where u.userName = :userName").setParameter("userName", userName);
		try {
			user = (UserEntity) query.getSingleResult();
		} catch(NoResultException e) {
			//do nothing
		}
		
		return user;
	}
}
