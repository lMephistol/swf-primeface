# Overview #
The application developed for view picture that can be uploaded/remove on specific image hosting([imgur](https://imgur.com) in this example). First of all you need registry. Than your data will be safe in database(MySQL) and password will be encrypted with bcrypt algorithm. After that you redirect to account page where you have an opportunity to view, upload or delete picture. The gallery hasn't any photo by default.  

# Used Technologies #
* Java 1.7 or newer.
* Spring core 4.0.9
* Spring web flow 2.3.4
* Spring security 3.2.8
* Mojarra 2.2
* Hibernate 4.1.8
* Primeface 5.2
* Apache commons(for file upload and DataSource)
* Jackson 1.9
* Maven
* Tomcat or other equivalent server.

**Work example: on [openshift](http://swfprimeface-armiua.rhcloud.com/)** 